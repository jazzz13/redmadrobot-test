//
//  DownloadManager.m
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "DownloadManager.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ImageObject.h"

@interface DownloadManager ()

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSMutableDictionary *currentDownloadUrls;

@end

@implementation DownloadManager

+ (instancetype)manager
{
    static DownloadManager *instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [self new];
        
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        _context = appDelegate.managedObjectContext;
        
        _currentDownloadUrls = [NSMutableDictionary new];
        
        _queue = [NSOperationQueue new];
        
        _queue.maxConcurrentOperationCount = 10;
    }
    
    return self;
}

- (void)fetchAllImagesFromId:(NSString *)vkId
                  complition:(void (^)(void))complition;
{
    NSString *urlPath = [NSString stringWithFormat:@"https://api.vk.com/method/photos.get?owner_id=%@&extended=1&album_id=profile&rev=1&v=5.27", vkId];
    NSURL *URL = [NSURL URLWithString:urlPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (!connectionError)
                               {
                                   [self parseResponseAndCreateImageObjects:data];
                               }
                               
                               if (complition)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       
                                       complition();
                                   });
                               }
                           }];
}

- (void)parseResponseAndCreateImageObjects:(NSData *)data
{
    NSManagedObjectContext *localContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    localContext.parentContext = _context;
    
    [localContext performBlock:^{
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data
                                                                  options:NSJSONReadingMutableContainers
                                                                    error:nil];
        
        if ([response isKindOfClass:[NSDictionary class]])
        {
            response = [response objectForKey:@"response"];
        }
        
        NSArray *items = nil;
        
        if ([response isKindOfClass:[NSDictionary class]])
        {
            items = [response objectForKey:@"items"];
        }
        
        for (NSDictionary* item in items)
        {
            ImageObject *imageObject = [NSEntityDescription insertNewObjectForEntityForName:@"ImageObject"
                                                                     inManagedObjectContext:localContext];
            
            imageObject.url = [item objectForKey:@"photo_130"];
            imageObject.date = [NSDate date];
            
            if ([items indexOfObject:item] % 10 == 9)
            {
                [localContext save:nil];
            }
        }
        
        [localContext save:nil];
    }];
}

- (void)downloadFileByUrl:(NSString *)url
               complition:(void (^)(NSString *))complition
{
    if([_currentDownloadUrls objectForKey:url] == nil)
    {
        [_currentDownloadUrls setObject:@YES
                                 forKey:url];
        
        [_queue addOperationWithBlock:^{
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            
            NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            path = [path stringByAppendingPathComponent:[url lastPathComponent]];
            
            [imageData writeToFile:path
                        atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_currentDownloadUrls removeObjectForKey:url];
                
                if(complition)
                {
                    complition(path);
                }
            });
        }];
    }
}

- (void)cancelAll
{
    [_queue cancelAllOperations];
}

@end
