//
//  LoginViewController.m
//  REDMADROBOT Test
//
//  Created by Rinat Murtazin on 07.12.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "LoginViewController.h"
#import "DownloadManager.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (IBAction)startButtonTapped:(UIButton *)sender;

@end

@implementation LoginViewController

- (IBAction)startButtonTapped:(UIButton *)sender
{
    NSString *vkId = _textField.text;
    
    vkId = [vkId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(vkId.length > 0)
    {
        [self enableLoadingUI];
        
        [[DownloadManager manager] fetchAllImagesFromId:vkId
                                             complition:^{
                                                 
                                                 [self performSegueWithIdentifier:@"imagesScreen"
                                                                           sender:self];
                                                 
                                             }];
        
        
    }
}

- (void)enableLoadingUI
{
    [_spinner startAnimating];
    _startButton.enabled = NO;
}


- (BOOL)shouldAutorotate
{
    return NO;
}

@end
