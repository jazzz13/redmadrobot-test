//
//  ImageCell.m
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "ImageCell.h"
#import "ImageObject.h"

static CGFloat animDuration = 0.3;

@interface ImageCell ()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *checkMark;

@end

@implementation ImageCell

- (void)configureByImageObject:(ImageObject *)imageObject
{
    [UIView animateWithDuration:animDuration
                     animations:^{
                         
                         self.checkMark.alpha = ([imageObject.selected boolValue] ? 1 : 0.2 );
                         
                     }];
    
    if(imageObject.localPath)
    {
        self.imageView.image = [UIImage imageWithContentsOfFile:imageObject.localPath];
        
        [UIView animateWithDuration:animDuration
                         animations:^{
                             
                             _imageView.alpha = 1;
                             _activityIndicatorView.alpha = 0;
                         }];
    }
    else
    {
        _imageView.alpha = 0;
        _activityIndicatorView.alpha = 1;
        
        [imageObject downloadFile];
    }
}

@end
