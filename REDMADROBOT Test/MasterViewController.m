//
//  MasterViewController.m
//  REDMADROBOT Test
//
//  Created by Rinat Murtazin on 08.12.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "MasterViewController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ImageObject.h"

@interface MasterViewController () <NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *collageButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation MasterViewController

- (NSFetchedResultsController *)fetchedResultsController
{
    if(!_fetchedResultsController)
    {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ImageObject"];
        
        [request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"selected"
                                                                    ascending:YES]]];
        
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:appDelegate.managedObjectContext
                                                                          sectionNameKeyPath:@"selected"
                                                                                   cacheName:nil];
        
        _fetchedResultsController.delegate = self;
        
        [_fetchedResultsController performFetch:nil];
    }
    
    return _fetchedResultsController;
}

- (void)updateUI
{
    int selected = 0;
    int all  = 0;
    
    id<NSFetchedResultsSectionInfo> firstSectionInfo = [[self.fetchedResultsController sections] firstObject];
    id<NSFetchedResultsSectionInfo> secondSectionInfo = nil;
    
    if([[self.fetchedResultsController sections] count] > 1)
    {
        secondSectionInfo = [self.fetchedResultsController sections][1];
    }
    
    if(firstSectionInfo)
    {
        ImageObject *firstObject = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        
        selected = ([firstObject.selected boolValue] ? [firstSectionInfo numberOfObjects] : [secondSectionInfo numberOfObjects] );
    }
    
    all = [firstSectionInfo numberOfObjects] + [secondSectionInfo numberOfObjects];
    
    _infoLabel.text = [NSString stringWithFormat:@"%d / %d", selected, all];
    
    
    _collageButton.enabled = selected > 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateUI];
}

#pragma mark - Actions

- (IBAction)selectButtonTapped:(id)sender
{
    NSManagedObjectContext *localContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSArray *allObjects = [[self.fetchedResultsController fetchedObjects] copy];
    
    localContext.parentContext = appDelegate.managedObjectContext;
    
    [localContext performBlock:^{
       
        for(ImageObject *image in allObjects)
        {
            if(![image.selected boolValue])
            {
                image.selected = @(YES);
            }
            
            if([allObjects indexOfObject:image] % 10 == 9)
            {
                [localContext save:nil];
            }
        }
        
        [localContext save:nil];
    }];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self updateUI];
}

@end
