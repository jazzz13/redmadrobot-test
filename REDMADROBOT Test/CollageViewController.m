//
//  CollageViewController.m
//  REDMADROBOT Test
//
//  Created by Rinat Murtazin on 12.12.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "CollageViewController.h"
#import <CoreData/CoreData.h>
#import "ImageObject.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>

CGFloat DegreesToRadians(CGFloat degrees)
{
    return degrees * M_PI / 180;
};

@interface CollageViewController () <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

- (IBAction)sendButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@end

@implementation CollageViewController
{
    BOOL _created;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(!_created)
    {
        [self startInitialization];
        
        _created = YES;
    }
}

- (void)startInitialization
{
    NSArray *imageObjects = [self fetchObjects];
    
    BOOL needWait = NO;
    
    for(ImageObject *imageObject in imageObjects)
    {
        if(!imageObject.localPath)
        {
            [imageObject downloadFile];
            
            needWait = YES;
        }
    }
    
    if(needWait)
    {
        __weak id weakSelf = self;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            __strong typeof(self) strongSelf = weakSelf;
            
            [strongSelf startInitialization];
        });
    }
    else
    {
        _imageView.image = [self createCollageWithImages:imageObjects];
        
        [self setupReadyUI];
    }
}

- (void)setupReadyUI
{
    [UIView animateWithDuration:0.4
                     animations:^{
                         
                         _imageView.alpha = 1;
                         _spinner.alpha = 0;
                     }];
    
    _sendButton.enabled = [MFMailComposeViewController canSendMail];
}

- (NSArray *)fetchObjects
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ImageObject"];
    
    [request setPredicate:[NSPredicate predicateWithFormat:@"selected = YES"]];
    
    return [appDelegate.managedObjectContext executeFetchRequest:request
                                                                            error:nil];
}

- (UIImage *)createCollageWithImages:(NSArray *)imageObjects
{
    CGFloat imageWidth = 130.0 - 15.0;
    int count = [imageObjects count];
    
    int columns = (int) sqrtf(count);
    
    if(columns < 2)
    {
        columns = 2;
    }
    
    CGSize size = CGSizeMake(imageWidth * columns, imageWidth * (count / columns));
    
    if(size.height < imageWidth)
    {
        size.height = imageWidth;
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for(int i = 0; i < count; i++)
    {
        ImageObject *imageObject = imageObjects[i];
        
        UIImage *image = [UIImage imageWithContentsOfFile:imageObject.localPath];
        
        if(image)
        {
            CGContextSaveGState(context);
            
            CGRect drawRect = {
                CGPointMake(imageWidth * (i % columns), imageWidth * (i / columns)) ,
                image.size
            };

            int degrees = (rand() % 31) - 15;
            
            CGContextTranslateCTM(context, CGRectGetMidX(drawRect), CGRectGetMidY(drawRect));
            
            CGContextRotateCTM(context, DegreesToRadians(degrees));
            
            CGContextTranslateCTM(context, -CGRectGetMidX(drawRect), -CGRectGetMidY(drawRect));
            ;
            
            [image drawInRect:drawRect];
            
            
            CGContextRestoreGState(context);
        }
    }
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return result;
}


- (IBAction)sendButtonTapped:(id)sender
{
    MFMailComposeViewController *mailVC = [MFMailComposeViewController new];
    
    mailVC.mailComposeDelegate = self;
    [mailVC setSubject:@"Photo collage"];
    
    [mailVC addAttachmentData:UIImagePNGRepresentation(_imageView.image)
                     mimeType:@"image/png"
                     fileName:@"collage.png"];
    
    [self presentViewController:mailVC
                       animated:YES
                     completion:nil];
}

- (IBAction)cancelButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result
                       error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

@end















